/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author de
 */
public class Xe implements Serializable{
    private int maXe;
    private String kieuXe;
    private int mucTienThue, soXe;
    
    private static int currentID = 999;

    public Xe() {
    }

    public Xe(String kieuXe, int mucTienThue, int soXe) {
        this.maXe = ++currentID;
        this.kieuXe = kieuXe;
        this.mucTienThue = mucTienThue;
        this.soXe = soXe;
    }

    public int getMaXe() {
        return maXe;
    }

    public String getKieuXe() {
        return kieuXe;
    }

    public int getMucTienThue() {
        return mucTienThue;
    }

    public int getSoXe() {
        return soXe;
    }

    public static int getCurrentID() {
        return currentID;
    }

    public static void setCurrentID(int currentID) {
        Xe.currentID = currentID;
    }
    
}
