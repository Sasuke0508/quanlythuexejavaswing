/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author de
 */
public class KhachHang implements Serializable{
    private int maKhach;
    private String tenKhach;
    private String loaiXeThue;
    private int soXeThue;
    
    private int currentID = 99;

    public KhachHang() {
    }

    public KhachHang(String tenKhach, String loaiXeThue, int soXeThue) {
        this.maKhach = ++currentID;
        this.tenKhach = tenKhach;
        this.loaiXeThue = loaiXeThue;
        this.soXeThue = soXeThue;
    }

    public int getMaKhach() {
        return maKhach;
    }

    public String getTenKhach() {
        return tenKhach;
    }

    public String getLoaiXeThue() {
        return loaiXeThue;
    }

    public int getSoXeThue() {
        return soXeThue;
    }

    public int getCurrentID() {
        return currentID;
    }

    public void setCurrentID(int currentID) {
        this.currentID = currentID;
    }
    
}
