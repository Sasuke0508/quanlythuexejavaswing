/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author de
 */
public class PhieuThue implements Serializable{
    private KhachHang khachHang;
    private Xe xe;
    private int soLuongThue, soNgayThue;

    public PhieuThue() {
    }

    public PhieuThue(KhachHang khachHang, Xe xe, int soLuongThue, int soNgayThue) {
        this.khachHang = khachHang;
        this.xe = xe;
        this.soLuongThue = soLuongThue;
        this.soNgayThue = soNgayThue;
    }

    public KhachHang getKhachHang() {
        return khachHang;
    }

    public Xe getXe() {
        return xe;
    }

    public int getSoLuongThue() {
        return soLuongThue;
    }

    public int getSoNgayThue() {
        return soNgayThue;
    }

    public void setSoNgayThue(int soNgayThue) {
        this.soNgayThue = soNgayThue;
    }

    public void setSoLuongThue(int soLuongThue) {
        this.soLuongThue = soLuongThue;
    }
    
}
